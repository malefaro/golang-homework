package main

import (
	"sort"
	"strconv"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(s []int) (result string) {
	for _, val := range s {
		result += strconv.Itoa(val)
	}
	return
}

func MergeSlices(s1 []float32, s2 []int32) (result []int) {
	result = make([]int, len(s1)+len(s2))
	var i int = 0
	for _, val := range s1 {
		result[i] = int(val)
		i++
	}
	for _, val := range s2 {
		result[i] = int(val)
		i++
	}
	return
}

func GetMapValuesSortedByKey(input map[int]string) []string {
	keys := make([]int, len(input))
	var i int = 0
	for key, _ := range input {
		keys[i] = key
		i++
	}
	sort.Ints(keys)
	i = 0
	result := make([]string, len(input))
	for _, key := range keys {
		result[i] = input[key]
		i++
	}
	return result
}