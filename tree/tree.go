package main

// сюда писать функцию DirTree

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func recDirTree(path string, prefix string, out io.Writer, flag bool) error {
	if path[len(path)-1] != '/' {
		path = path + "/"
	}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	if !flag {
		buffer := make([]os.FileInfo, 0, 0)
		for _, file := range files {
			if file.IsDir() {
				buffer = append(buffer, file)
			}
		}
		files = buffer
	}

	for index, file := range files {
		nextfile := path + file.Name()
		line := ""
		tab := ""
		if index != len(files)-1 {
			line = "├───"
			tab = "│\t"
		} else {
			line = "└───"
			tab = "\t"
		}
		switch {
		case !file.IsDir() && file.Size() != 0:
			fmt.Fprintf(out, "%s%s%s (%db)\n", prefix, line, file.Name(), file.Size())
		case file.Size() == 0:
			fmt.Fprintf(out, "%s%s%s (empty)\n", prefix, line, file.Name())
		default:
			fmt.Fprintf(out, "%s%s%s\n", prefix, line, file.Name())
		}
		if !file.IsDir() {
			continue
		}
		err = recDirTree(nextfile, prefix+tab, out, flag)
		if err != nil {
			return err
		}
	}
	return nil
}

func dirTree(out io.Writer, path string, flag bool) error {
	err := recDirTree(path, "", out, flag)
	return err
}
