package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	//"time"
)

func ExecutePipeline(jobs ...job) {
	wg := &sync.WaitGroup{}
	in := make(chan interface{})
	for _, OneJob := range jobs {
		wg.Add(1)
		out := make(chan interface{})
		/*здесь необходимо передавать текущую функцию, брать из замыкания нельзя, иначе на момент исполнения будет уже другая функция (??)*/
		go func(in, out chan interface{}, currentJob job) {
			defer wg.Done()
			defer close(out)
			currentJob(in, out)
		}(in, out, OneJob)
		in = out
	}
	wg.Wait()
}

func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for i := range in {
		wg.Add(1)
		data := strconv.Itoa(i.(int))
		fmt.Println(data, "SingleHash data", data)
		md5 := DataSignerMd5(data)
		fmt.Println(data, "SingleHash md5(data)", md5)

		go func(PureData, DataMd5 string) {
			defer wg.Done()
			chData := make(chan string)
			chMd5 := make(chan string)
			calc := func(data string, ch chan string) {
				ch <- DataSignerCrc32(data)
			}
			go calc(PureData, chData)
			go calc(DataMd5, chMd5)
			crc32data := <-chData
			crc32md5data := <-chMd5
			result := crc32data + "~" + crc32md5data
			fmt.Println(data, "SingleHash crc32(md5(data))", crc32md5data)
			fmt.Println(data, "SingleHash crc32(data)", crc32md5data)
			fmt.Println(data, "SingleHash result)", result)
			// out <- (<-chData) + "~" + (<-chMd5)
			out <- result

		}(data, md5)

	}
	wg.Wait()
}

func MultiHash(in, out chan interface{}) {
	const th = 6
	wgMultiHash := &sync.WaitGroup{}
	for i := range in {
		wgMultiHash.Add(1)
		go func() {
			defer wgMultiHash.Done()
			wgCalc := &sync.WaitGroup{}
			var multiHashes []string = make([]string, th)
			for index := 0; index < th; index++ {
				wgCalc.Add(1)
				go func(array []string, index int, data interface{}) {
					defer wgCalc.Done()
					array[index] = DataSignerCrc32(strconv.Itoa(index) + data.(string))
					fmt.Printf("%v MultiHash: crc32(th+step1)) %v %v\n", data, index, array[index])
				}(multiHashes, index, i)
			}
			wgCalc.Wait()
			var result string = multiHashes[0]
			for i := 1; i < th; i++ {
				result += multiHashes[i]
			}
			out <- result
		}()
	}
	wgMultiHash.Wait()
}

func CombineResults(in, out chan interface{}) {
	resultsArray := make([]string, 0)
	for i := range in {
		resultsArray = append(resultsArray, i.(string))
	}
	sort.Strings(resultsArray)
	var result string = strings.Join(resultsArray, "_")
	out <- result
}

// func main() {

// 	inputData := []int{0, 1, 1, 2, 3, 5, 8}
// 	// inputData := []int{0,1}
// 	testResult := "NOT_SET"
// 	hashSignJobs := []job{
// 		job(func(in, out chan interface{}) {
// 			for _, fibNum := range inputData {
// 				out <- fibNum
// 			}
// 		}),
// 		job(SingleHash),
// 		job(MultiHash),
// 		job(CombineResults),
// 		job(func(in, out chan interface{}) {
// 			dataRaw := <-in
// 			data, ok := dataRaw.(string)
// 			if !ok {
// 				fmt.Println("cant convert result data to string")
// 			}
// 			testResult = data
// 		}),
// 	}

// 	start := time.Now()

// 	ExecutePipeline(hashSignJobs...)

// 	end := time.Since(start)

// 	fmt.Println("working time", end)
// }
